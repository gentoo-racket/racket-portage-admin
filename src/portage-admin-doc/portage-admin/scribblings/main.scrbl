;; This file is part of racket-portage-admin - GUI Portage administration tool.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-portage-admin is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-portage-admin is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-portage-admin.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require portage-admin/version)


@title[#:tag "portage-admin"]{Racket-Portage-Admin}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]


A GUI application for inspection of Gentoo-based systems.

Version: @VERSION


@table-of-contents[]


@include-section{cmdline.scrbl}
@include-section{modules.scrbl}


@index-section[]
