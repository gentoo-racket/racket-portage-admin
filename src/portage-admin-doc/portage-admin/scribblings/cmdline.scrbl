;; This file is part of racket-portage-admin - GUI Portage administration tool.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-portage-admin is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-portage-admin is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-portage-admin.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "portage-admin-cmdline"]{Console usage}


@itemlist[
 @item{
  @Flag{m} or @DFlag{main}
  --- show the main window
 }
 @item{
  @Flag{e} or @DFlag{eselect}
  --- show the eselect modules explorer
 }
 @item{
  @Flag{l} or @DFlag{log}
  --- show the log files explorer
 }
 @item{
  @Flag{p} or @DFlag{pkgdb}
  --- show the package database explorer
 }

 @item{
  @Flag{h} or @DFlag{help}
  --- show help information with usage options
 }
 @item{
  @Flag{V} or @DFlag{version}
  --- show program version
 }
 ]
