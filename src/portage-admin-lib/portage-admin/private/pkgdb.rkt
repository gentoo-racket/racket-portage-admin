;; This file is part of racket-portage-admin - GUI Portage administration tool.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-portage-admin is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-portage-admin is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-portage-admin.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/file
         threading
         "q-wrapper.rkt")

(provide (all-defined-out)
         pkgdb)


(define pkg-attributes
  '("BDEPEND"
    "BUILD_TIME"
    "CATEGORY"
    "CBUILD"
    "CFLAGS"
    "CHOST"
    "CONTENTS"
    "COUNTER"
    "CXXFLAGS"
    "DEFINED_PHASES"
    "DEPEND"
    "DESCRIPTION"
    "EAPI"
    "FEATURES"
    "HOMEPAGE"
    "INHERITED"
    "IUSE"
    "IUSE_EFFECTIVE"
    "KEYWORDS"
    "LDFLAGS"
    "LICENSE"
    "NEEDED"
    "PF"
    "PKGUSE"
    "PROVIDES"
    "RDEPEND"
    "REQUIRES"
    "SIZE"
    "SLOT"
    "USE"))


(define (get-all-pkgs [pkgdb-path (pkgdb)])
  (define required-file "EAPI")
  (~> pkgdb-path
      (directory-list _ #:build? #t)
      (map (lambda~> (directory-list _ #:build? #t)
                     (map (lambda~> (build-path _ required-file)) _)
                     (filter file-exists? _))
           _)
      (apply append _)
      (map (lambda~> path->string
                     (regexp-replace (path->string pkgdb-path) _ "")
                     (regexp-replace required-file _ "")
                     (regexp-replace #rx"^/" _ "")
                     (regexp-replace #rx"/$" _ ""))
           _)))

(define (regexp-pkg-match-all rx [pkgs-list (get-all-pkgs)])
  (filter (lambda (str) (regexp-match rx str)) pkgs-list))

(define (regexp-pkg-match . vs)
  (define m (apply regexp-pkg-match-all vs))
  (if (null? m) #f (car m)))


(define (category/package [pkgs-list (get-all-pkgs)])
  (map (lambda (s)
         (define l (regexp-split "/" s))
         (hash 'category (car l) 'package (cadr l)))
       pkgs-list))

(define (attribute-file-contents attr pkg [pkgdb-path (pkgdb)])
  (let ([fp (build-path pkgdb-path pkg attr)])
    (if (file-exists? fp)
        (~> fp file->string (regexp-replace #rx"\n$" _ ""))
        "")))
