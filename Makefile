MAKE            ?= make
RACKET          := racket
RACO            := $(RACKET) -l raco --
SCRIBBLE        := $(RACO) scribble

PWD             ?= $(shell pwd)
BIN             := $(PWD)/bin
DOCS            := $(PWD)/docs
EXTRAS          := $(PWD)/extras
PUBLIC          := $(PWD)/public
SCRIPTS         := $(PWD)/scripts
SRC             := $(PWD)/src
ENTRYPOINT      := $(SRC)/portage-admin-lib/portage-admin/main.rkt
APPS-DIR        := ${HOME}/.local/share/applications


.PHONY: all
all: compile

src-%:
	$(MAKE) -C src $(*)

.PHONY: clean
clean: src-clean

.PHONY: compile
compile: src-compile

.PHONY: install-launcher
install-launcher:
	mkdir -p $(APPS-DIR)
	cp $(EXTRAS)/racket-portage-admin.desktop $(APPS-DIR)

.PHONY: install
install: src-install install-launcher

.PHONY: setup
setup: src-setup

.PHONY: shellcheck
shellcheck:
	find $(PWD) -type f -name "*.sh" -exec shellcheck {} +

.PHONY: test
test: src-test

.PHONY: remove
remove: src-remove

$(PUBLIC):
	$(SH) $(SCRIPTS)/public.sh

.PHONY: public
public:
	$(MAKE) -B $(PUBLIC)

$(BIN):
	mkdir -p $(BIN)
	$(RACO) exe -v -o $(BIN)/portage-admin $(ENTRYPOINT)

.PHONY: bin
bin:
	$(MAKE) -B $(BIN)
