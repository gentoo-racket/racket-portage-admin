# Racket Portage Admin


## About

A GUI application for inspection of Gentoo-based systems.


## Screenshots

![version 1.3.0](extras/images/screenshots/1.3.0.png "version 1.3.0")


## Installation

To install, execute:
```bash
make install
```

### Dependencies

- threading-lib

### Subpackages

| name               | role                               |
|--------------------|------------------------------------|
| portage-admin      | metapackage                        |
| portage-admin-doc  | documentation of the whole project |
| portage-admin-lib  | core library                       |
| portage-admin-test | tests                              |

To install a subpackage go it it's directory and execute `raco pkg install`.


## Running

To run without installation ensure you have
the dependencies installed (listed above),
and execute:
```bash
make compile
sh ./scripts/main.sh
```


## License

This file is part of racket-portage-admin - GUI Portage administration tool.

Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
